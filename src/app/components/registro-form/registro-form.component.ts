import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import axios from 'axios';
import { Router } from '@angular/router';

//import {conekta} from 'conekta';
//import { conekta } from 'conekta';



@Component({
  selector: 'app-registro-form',
  templateUrl: './registro-form.component.html',
  styleUrls: ['./registro-form.component.scss']
})
export class RegistroFormComponent implements OnInit {

  personalInfo:FormGroup;
  objetivosInfo:FormGroup;
  medicoInfo:FormGroup;
  reglasInfo:FormGroup;
  derechoInfo:FormGroup;

  alumno:Object = {

    direccionCompleta: {
      street: "",
      ext: "",
      int: "",
      colonia: ""
    },
    entrenamiento: ""
  }

  entrenamientos = [{
    codigo: "intro-101",
    nombre: "INTRO 101"
  },
  {
    codigo: "intro-102",
    nombre: "INTRO 102"
  }];

  objetivosLabel:string[] = ["Primer objetivo", "Segundo objetivo", "Tercer objetivo"];

  objetivosArray:any[] = [];


  mostrar:boolean = false;
  show:boolean = false;
  show2:boolean = false;
  show3:boolean = false;
  show4:boolean = false;
  show5:boolean = false;
  showCarta:boolean = false;
  showPregnant:boolean = true;
  objetivosDisabled:boolean = false;

  /* modelos para los radios */
  adulto:boolean;
  enfermedad:boolean ;
  alergia:boolean;
  embarazada:boolean;
  droga:boolean;
  medicamento:boolean;
  tratamiento:boolean;
  crisis__nerviosa:boolean;
  suicidio:boolean;

  fecha_nacimiento:string = "";

  bCard:boolean;

  _id:number =  0; // ID del usuario
  id_alumno:number = 0; // id del alumno

  selectedTipoPago:number = 0;  // pago únic, diferido...

  isDisabled:boolean = false;
  enviarTxt:string = "Enviar";

  constructor( private router: Router ) {
      //console.log( conekta );
      this.fecha_nacimiento = "";
  }

  agregarObjetivo(){
    (<FormArray>this.objetivosInfo.controls['objetivos']).push(
      new FormControl()
    )

    if (this.objetivosInfo.value.objetivos.length > 2 ){
      this.objetivosDisabled = true;
    }
  }

  guardarPersonalInfo(){
    // console.log(this.personalInfo.value.birthdate);
     if(this.personalInfo.valid === true){


     }
  }

  guardarObjetivosInfo(){
    //console.log(this.objetivosInfo.value);
  }


  guardarMedicoInfo(){
    // console.log(this.medicoInfo);
    // console.log(this.medicoInfo.value);
    if(this.adulto === true){
      this.bCard = true;
    } else {
      this.bCard = false;
    }


  }

  guardarAvisodePrivacidad(){


  }

  guardarDerecho(){
    let that = this;
      //console.log( this.objetivosInfo.value.objetivos[0] );
      console.log( "Datos del archivo --- >" );
      this.isDisabled = true;
      this.enviarTxt = "Enviando...";
     ////////////////////////////
     const _datos_archivote = this.medicoInfo.value.archivote;
     const _datos_ine = this.medicoInfo.value.ine;
     //console.log( formModel );

      let Objetivo1 = "";
      let Objetivo2 = "";
      let Objetivo3 = "";

      //this.router.navigate(['/selecciona-pago/150']);


        // Se obtienen los objetivos
        // {
        try { Objetivo1 = this.objetivosInfo.value.objetivos[0] } catch(err) { }
        try { Objetivo2 = this.objetivosInfo.value.objetivos[1] } catch(err) { }
        try { Objetivo3 = this.objetivosInfo.value.objetivos[2] } catch(err) { }
        //}

    //console.log( this.medicoInfo.value );
    //console.log( this.medicoInfo.value.enfermedad__especifica );


      axios.post('//misionmexico.com/api/register',
      {
          name: this.personalInfo.value.name,
          lastname: this.personalInfo.value.lastname,
          nickname: this.personalInfo.value.nickname,
          who__invited: this.personalInfo.value.who__invited,
          who__invited_cellphone: this.personalInfo.value.who__invited_cellphone,
          birthdate: this.fecha_nacimiento,
          occupation: this.personalInfo.value.occupation,
          street: this.personalInfo.value.direccionCompleta.street,
          ext: this.personalInfo.value.direccionCompleta.ext,
          int: this.personalInfo.value.direccionCompleta.int,
          Colonia: this.personalInfo.value.direccionCompleta.colonia,
          phone__home: this.personalInfo.value.phone__home,
          phone__cell: this.personalInfo.value.phone__cell,
          office: this.personalInfo.value.office,
          email: this.personalInfo.value.email,
          entrenamiento: this.personalInfo.value.entrenamiento,

          resultado__esperado_1: Objetivo1,
          resultado__esperado_2: Objetivo2,
          resultado__esperado_3: Objetivo3,

          emergency__name_1: this.objetivosInfo.value.emergency__name_1,
          emergency__phone_1: this.objetivosInfo.value.emergency__phone_1,
          emergency__relation_1: this.objetivosInfo.value.emergency__relation_1,
          emergency__name_2: this.objetivosInfo.value.emergency__name_2,
          emergency__phone_2: this.objetivosInfo.value.emergency__phone_2,
          emergency__relation_2: this.objetivosInfo.value.emergency__relation_2,


          adulto: this.adulto, //this.medicoInfo.value.adulto,
          enfermedad: this.enfermedad,
          enfermedad__especifica: this.medicoInfo.value.enfermedad__especifica,
          enfermedad__medicamento: this.medicoInfo.value.enfermedad__medicamento,
          enfermedad__fecha: this.medicoInfo.value.enfermedad__fecha,

          alergia: this.alergia,
          alergia__especifica: this.medicoInfo.value.alergia__especifica,
          alergia__reaccion: this.medicoInfo.value.alergia__reaccion,

          embarazada: this.embarazada,

          droga: this.droga,
          droga__especifica: this.medicoInfo.value.droga__especifica,
          droga__cuando: this.medicoInfo.value.droga__cuando,
          droga__tratamiento: this.medicoInfo.value.droga__tratamiento,

          medicamento: this.medicamento,
          medicamento__especifica: this.medicoInfo.value.medicamento__especifica,
          medicamento__porque: this.medicoInfo.value.medicamento__porque,

          tratamiento: this.tratamiento,

          crisis__nerviosa: this.crisis__nerviosa,
          crisis__nerviosa_tipo: this.medicoInfo.value.crisis__nerviosa_tipo,
          crisis__nerviosa_cuando: this.medicoInfo.value.crisis__nerviosa_cuando,
          crisis__nerviosa_tratamiento: this.medicoInfo.value.crisis__nerviosa_tratamiento,

          suicidio: this.suicidio,
          suicidio_suicidio_cuando: this.medicoInfo.value.suicidio_cuando,
          suicidio_porque: this.medicoInfo.value.suicidio_porque,
          suicidio_tratamiento: this.medicoInfo.value.suicidio_tratamiento,
          archivo: _datos_archivote,
          archivo_ine: _datos_ine,

            }

            )
            .then( (response) => {
                console.log( "respuesta registro", response );
                this.id_alumno = response.data.id;
                this.router.navigate(['/selecciona-pago/' + this.id_alumno]);
            })
            .catch(function (error) {
              // si hubo error
              console.log( "Ocurrió algún error CHIN!", error );
              that.isDisabled = false;
              that.enviarTxt = "Enviar";
            });


  }

  ngOnInit() {

    this.personalInfo = new FormGroup({

      'direccionCompleta': new FormGroup({
          'street': new FormControl(),
          'ext': new FormControl(),
          'int': new FormControl(),
          'colonia': new FormControl()
      }),

      'name': new FormControl(),
      'lastname': new FormControl(),
      'nickname': new FormControl(),
      'who__invited': new FormControl(),
      'who__invited_cellphone': new FormControl(),
      'birthdate': new FormControl(),
      'occupation': new FormControl(),
      'phone__home': new FormControl(),
      'phone__cell': new FormControl(),
      'office': new FormControl(),
      'email': new FormControl(),
      'entrenamiento': new FormControl()

    })

    this.objetivosInfo = new FormGroup({

      'objetivos': new FormArray([
        new FormControl()
      ]),

      'emergency__name_1': new FormControl(),
      'emergency__phone_1': new FormControl(),
      'emergency__relation_1': new FormControl(),
      'emergency__name_2': new FormControl(),
      'emergency__phone_2': new FormControl(),
      'emergency__relation_2': new FormControl()


    })

    this.medicoInfo = new FormGroup({
      'adulto': new FormControl('', Validators.required ),
      'enfermedad': new FormControl(),
      'enfermedad__especifica': new FormControl(),
      'enfermedad__medicamento': new FormControl(),
      'enfermedad__fecha': new FormControl(),
      'alergia': new FormControl(),
      'alergia__especifica': new FormControl(),
      'alergia__reaccion': new FormControl(),
      'embarazada': new FormControl(),
      'droga': new FormControl(),
      'droga__especifica': new FormControl(),
      'droga__cuando': new FormControl(),
      'droga__tratamiento': new FormControl(),
      'medicamento': new FormControl(),
      'medicamento__especifica': new FormControl(),
      'medicamento__porque': new FormControl(),
      'tratamiento': new FormControl(),
      'crisis__nerviosa': new FormControl(),
      'crisis__nerviosa_tipo': new FormControl(),
      'crisis__nerviosa_cuando': new FormControl(),
      'crisis__nerviosa_tratamiento': new FormControl(),
      'suicidio': new FormControl(),
      'suicidio_cuando': new FormControl(),
      'suicidio_porque': new FormControl(),
      'suicidio_tratamiento': new FormControl(),
      'archivote': new FormControl(),
      'ine': new FormControl()
    })

    this.reglasInfo = new FormGroup({
      "aviso_de_privacidad": new FormControl(),
      "reglas_de_participacion": new FormControl()
    })

    this.derechoInfo = new FormGroup({
      "derecho_de_exclusion": new FormControl()
    })

    let that = this;

    // Se obtienen los entrenamientos
    axios.get('./api/getEntrenamientos', {

        })
        .then( (response) => {
            const _data = response.data;
            that.entrenamientos = _data;
    })
    .catch(function (error) {
        console.log( "Ocurrió algún error CHIN! (generateOxxoPDF)", error );
    });

  }

  cambia_fecha(){
      console.log( this.fecha_nacimiento );
  }

  onKeyFecha(event: any) {

      console.log ( "keyCode",event.keyCode );

      if( event.keyCode != 8 )
      {
          if( this.fecha_nacimiento.length == 4 || this.fecha_nacimiento.length == 7 )
          {
              this.fecha_nacimiento += '-';
          }
      }

      // Si es mayor de 10 se eliiina la entrada
      if( this.fecha_nacimiento.length > 10 )
      {
          this.fecha_nacimiento = this.fecha_nacimiento.substring(0, 10);
      }
      if( event.keyCode < 96  || event.keyCode > 105 )
      {
          // this.fecha_nacimiento = this.fecha_nacimiento.substring(0, this.fecha_nacimiento.length-1 );
      }

    console.log( this.fecha_nacimiento );
  }

  clearFile(){
    (<HTMLInputElement>document.getElementById('archivote')).value = null;
  }

  clearFileINE(){
    (<HTMLInputElement>document.getElementById('ine')).value = null;
  }

  createForm() {

  }

  onFileChange(event) {
      console.log( "Leyendo archivo" );

        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.medicoInfo.get('archivote').setValue({
              filename: file.name,
              filetype: file.type,
              value: reader.result.split(',')[1]
            })
          };
        }
    }

  onINEChange(event) {
      console.log( "Leyendo archivo" );

    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      console.log(this.medicoInfo.value);
      reader.onload = () => {
        this.medicoInfo.get('ine').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })
      };
    }
  }

}
