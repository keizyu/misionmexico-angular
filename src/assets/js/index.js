
jQuery.extend(jQuery.validator.messages, {
    required: "",
		email: "Por favor, introduce un email válido",
    digits: "Por favor, introduce una fecha de nacimiento válida",
    maxlength: jQuery.validator.format("Por favor, ingrese no más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, ingrese su número a {0} digitos"),
    // max: jQuery.validator.format("Por favor, ingrese su número a 10 digitos")
});
