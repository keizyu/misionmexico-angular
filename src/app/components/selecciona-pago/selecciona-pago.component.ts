import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import axios from 'axios';
import swal from 'sweetalert';

declare var Conekta:any; // libería de conekta

@Component({
    selector: 'app-selecciona-pago',
    templateUrl: './selecciona-pago.component.html',
    styleUrls: ['./selecciona-pago.component.scss']
})

export class SeleccionaPagoComponent implements OnInit {
    private idAlumno:number = 0;
    private idAlumnoPadre:number = 0;
    private existeAlumno:boolean = false;
    private entrenamiento_id:number= 0;

    private _matricula:string = "";
    private _entrenamiento:string = "";
    private _nombre:string = "";
    private _apellido:string = "";
    private sub: any;

    private _monto_tdc:number = 0;
    private _monto_oxxo:number = 0;
    private _monto_oficinas:number = 0; // precio oficina = oficina banco
    private _monto_banco:number = 0;    // precio oficina = oficina banco

    private bPagoTDC:boolean = true;
    private bPagoOficinas: boolean = false;
    private bPagoDeposito: boolean = false;
    private bPagoOxxo: boolean = false;

    // datos del pago de OXXO
    private _oxxo_monto:number = 0;
    private _oxxo_referencia:number = 0;

    // datos del pago con TDC
    private _tdc_tipo_de_pago = [{
      value: "pago_unico",
      name: "Pago único"
    },
    {
      value: "pago_6_meses",
      name: "6 meses sin intereses"
    },
    {
      value: "pago_12_meses",
      name: "12 meses sin intereses"
    }];

    private _tdc_nombre_del_titular:string = "";
    private _tdc_numero_de_tarjeta:string = "";
    private _tdc_vencimiento_mm:string = "";
    private _tdc_vencimiento_aa:string = "";
    private _tdc_cvv:string = "";

    private showPagoUnico:boolean = true;
    private showMonto6meses:boolean = false;
    private showMonto12meses:boolean = false;


    // se inhabilitan los botones
    private isDisabled:boolean = false;

    // si es el primer registro
    private primerPago:boolean = false;
    private pagoParcial:boolean = false;

    // Parcial Login
    parcialLogin:FormGroup;
    entrenamientos = [{
      codigo: "intro-101",
      nombre: "INTRO 101"
    },
    {
      codigo: "intro-102",
      nombre: "INTRO 102"
    }];

    // Paso 2
    private bPaso2_oxxo:boolean = false;
    private bPaso2_oficina:boolean = false;

    private hideForm:boolean = false;

    constructor( private route: ActivatedRoute ) {
        // Modo de pruebas
        //////Conekta.setPublicKey("key_EyHV5U31h9r1ZsFiYBrdkGw");
        //Conekta.setPublicKey("key_eYvWV7gUaMyaN4iD");

        Conekta.setPublicKey("key_S1VUbB2vokjgLUjc4uZzCUg");
        //Conekta.setPublicKey("key_KJysdbf6PotS2ut2");
    }

    submitParcialLogin(){
        let that = this;

        console.log(this.parcialLogin.value);

        if(this.parcialLogin.valid === true){
            axios.get('./api/getStudentData/?matricula=' + that.parcialLogin.value.matricula_login + "&correo=" + that.parcialLogin.value.email_login, {})
                .then( (response) => {
                    const _data = response.data;

                    if( _data == null )
                    {
                        // Mostrar mensaje de que no se encontró el usuario
                        console.log( "valor nulo" );
                        swal("Email y/o matrícula inválida");
                    }
                    else
                    {
                        console.log( _data );

                        this.idAlumno = _data.id;
                        this.existeAlumno = true;
                        this.primerPago = false;
                        this.pagoParcial = true;
                        this.idAlumnoPadre = _data.id;
                        this.entrenamiento_id = that.parcialLogin.value.entrenamiento;
                        this._nombre = _data.nombre;
                        this._apellido = _data.apellidos;

                        //console.log( "Alumno", this.idAlumno );
                        //console.log( "bPaso2_oxxo", this.bPaso2_oxxo );

                        // Mostrar formulario de pago


                        axios.get('./api/getAmount/?id=' + this.idAlumno, {})
                            .then( (response) => {
                                const _data = response.data;

                                this._monto_tdc = _data.monto_tdc;
                                this._monto_oficinas = _data.monto_oficina;
                                this._monto_oxxo = _data.monto_oxxo;
                                this._monto_banco = _data.monto_oficina;
                                this._matricula = _data.matricula;
                                this._entrenamiento = _data.entrenamiento_id;
                        })
                        .catch(function (error) {
                            console.log( "Ocurrió algún error! (getDataStudent)", error );
                        });

                        //console.log( "----------------------------------" );
                    }
                    /*
                    this._monto_tdc = _data.monto_tdc;
                    this._monto_oficinas = _data.monto_oficina;
                    this._monto_oxxo = _data.monto_oxxo;
                    this._monto_banco = _data.monto_oficina;
                    this._matricula = _data.matricula;
                    this._entrenamiento = _data.entrenamiento_id;
                    */
            })
            .catch(function (error) {
                console.log( "Ocurrió algún error! (getAmount)", error );
            });
        }
    }

    ngOnInit() {

        this.parcialLogin = new FormGroup({
          'email_login': new FormControl(),
          'matricula_login': new FormControl(),
          'entrenamiento': new FormControl()
        });

        this.sub = this.route.params.subscribe(params => {
            console.log(this.idAlumno);
            this.idAlumno = +params['id'];

            if(  isNaN( this.idAlumno ) )
            {
                this.idAlumno = 0
            }
            else
            {
                this.existeAlumno=true;
                this.primerPago = true;
                this.pagoParcial = false;
            }

            //console.log( this.existeAlumno )
            //console.log( this.idAlumno );
        });

        // Se obtiene el monto que debe pagaar el usuario
        //  dependiendo del id del usuario
        axios.get('./api/getAmount/?id=' + this.idAlumno, {})
            .then( (response) => {
                const _data = response.data;

                this._monto_tdc = _data.monto_tdc;
                this._monto_oficinas = _data.monto_oficina;
                this._monto_oxxo = _data.monto_oxxo;
                this._monto_banco = _data.monto_oficina;
                this._matricula = _data.matricula;
                this._entrenamiento = _data.entrenamiento_id;
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (getAmount)", error );
        });

        // Se obtienen los entrenamientos
        axios.get('./api/getEntrenamientos', {

            })
            .then( (response) => {
                const _data = response.data;
                this.entrenamientos = _data;
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (getEntrenamientos)", error );
        });
    }

    TDC_on()
    {
        if ( !this.bPagoTDC )
        {
            this.bPagoTDC = true;
            this.bPagoOficinas = false;
            this.bPagoDeposito = false;
            this.bPagoOxxo = false;
        }

        // Si es parcialidad no se actualiza el tipo de pago
        if( this.idAlumnoPadre >= 0 )
        {
            return;
        }

        axios.get('./api/update_pay/?id=' + this.idAlumno + "&tipo_pago=" + 1, {})
            .then( (response) => {
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (getAmount)", error );
        });
    }

    Oxxo_on()
    {
        if ( !this.bPagoOxxo )
        {
            this.bPagoOxxo = true;
            this.bPagoOficinas = false;
            this.bPagoDeposito = false;
            this.bPagoTDC = false;
        }

        // Si es parcialidad no se actualiza el tipo de pago
        if( this.idAlumnoPadre >= 0 )
        {
            return;
        }

        axios.get('./api/update_pay/?id=' + this.idAlumno + "&tipo_pago=" + 2, {})
            .then( (response) => {
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (getAmount)", error );
        });
    }

    Oficinas_on()
    {
        if ( !this.bPagoOficinas )
        {
            this.bPagoOficinas = true;
            this.bPagoOxxo = false;
            this.bPagoDeposito = false;
            this.bPagoTDC = false;
        }

        // Si es parcialidad no se actualiza el tipo de pago
        if( this.idAlumnoPadre >= 0 )
        {
            return;
        }

        axios.get('./api/update_pay/?id=' + this.idAlumno + "&tipo_pago=" + 3, {})
            .then( (response) => {
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (getAmount)", error );
        });
    }

    Deposito_on()
    {
        if ( !this.bPagoDeposito )
        {
            this.bPagoDeposito = true;
            this.bPagoOxxo = false;
            this.bPagoOficinas = false;
            this.bPagoTDC = false;
        }

        // Si es parcialidad no se actualiza el tipo de pago
        if( this.idAlumnoPadre >= 0 )
        {
            return;
        }

        axios.get('./api/update_pay/?id=' + this.idAlumno + "&tipo_pago=" + 4, {})
            .then( (response) => {
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (getAmount)", error );
        });
    }

    /* Método para generar la orden de pago en oxxo */
    generaOrdenOxxo()
    {
        let that = this;
        this.isDisabled = true;

        axios.post('./api/dopaymentOxxo', {
                id: this.idAlumno,
                idPadre: this.idAlumnoPadre,
                entrenamiento_id: this.entrenamiento_id
            })
            .then( (response) => {

                if( response.data.error == 0 )
                {
                    console.log( response.data );
                    const _data = response.data;

                    console.log( "oxxo", response );

                    this._oxxo_monto = _data.amount;
                    this._oxxo_referencia = _data.reference;
                    this.bPaso2_oxxo = true;
                }
                else
                {
                    swal( response.data.reference );
                }
        })
        .catch(function (error) {
            console.log( "Ocurrió algún error CHIN! (generateOxxoPDF)", error );
            that.isDisabled = false;
        });
    }

    selectedTipoPago:string;

    onChange(newValue) {

        console.log(newValue);
        this.tipo_de_pago = newValue;

        if (newValue == "pago_unico" ){
            this.showPagoUnico = true;
            this.showMonto6meses = false;
            this.showMonto12meses = false;
        }

        if (newValue == "pago_6_meses" ){
            this.showPagoUnico = false;
            this.showMonto6meses = true;
            this.showMonto12meses = false;

        }
        if (newValue == "pago_12_meses" ){
            this.showPagoUnico = false;
            this.showMonto6meses = false;
            this.showMonto12meses = true;
        }
    }

    tipo_de_pago:any;

    /* Método para realizar el pago con TDC */
    procesaPagoTDC()
    {
        this.isDisabled = true;
        var tipo_pago = this.tipo_de_pago;

        const data = {
          "card": {
            "number": this._tdc_numero_de_tarjeta ,
            "name": this._tdc_nombre_del_titular,
            "exp_year": this._tdc_vencimiento_aa,
            "exp_month": this._tdc_vencimiento_mm,
            "cvc": this._tdc_cvv,
            "tipo": tipo_pago,
          }
        };


        // Se genera el token de conekta
        Conekta.Token.create( data, ( msg ) => {
            if( msg ) {
                console.log( "token: ", msg.id, "id_alumno: ", this.idAlumno);

                let that = this;

                // Aquí el token es correcto
                axios.post('./api/dopayment/', {
                    _token: msg.id,
                    _id_alumno: that.idAlumno,
                    tipo: tipo_pago,
                    _id_alumno_padre: that.idAlumnoPadre,
                    entrenamiento_id: this.entrenamiento_id
                })
                .then(function (response) {
                    console.log(response);

                    // Si no hay errores
                    if( response.data.error == "0" )
                    {
                        //that.divMessage.html( "Pago procesado correctamente." );
                        swal( "Tu pago ha sido procesado correctamente" );
                        //Se oculta el formulario
                        that.hideForm = true;
                        that.isDisabled = true;
                    }
                    else
                    {
                        swal( "Ocurrió algún error: \n\n" + response.data.description );
                        console.log( "error aquí" );
                        that.isDisabled = false;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    swal( error.message_to_purchaser );
                    console.log( error.message_to_purchaser );
                    that.isDisabled = false;
                });
            }
        }, ( msgError ) => {
            console.log( msgError );
            swal( msgError.message_to_purchaser );
            this.isDisabled = false;
        } );
    }

    // Se abre ventana apra descargar el PDF
    gotoPDF()
    {
        window.open("/api/getPDF/?id=" + this.idAlumno );
    }
}
