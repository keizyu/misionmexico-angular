import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { provideRoutes} from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SeleccionaPagoComponent } from './components/selecciona-pago/selecciona-pago.component';
import { PagoComponent } from './components/pago/pago.component';
import { RegistroFormComponent } from './components/registro-form/registro-form.component';
import { InicioComponent } from './components/inicio/inicio.component';


////////////////////////////////
const appRoutes: Routes = [
    { path: 'registro', component: RegistroFormComponent },
    { path: 'selecciona-pago', component: SeleccionaPagoComponent },
    { path: 'selecciona-pago/:id', component: SeleccionaPagoComponent },
    { path: 'pago', component: PagoComponent },
    { path: '',
    component: InicioComponent,
    pathMatch: 'full'
    },
];
////////////////////////////////

@NgModule({
  declarations: [
    AppComponent,
    SeleccionaPagoComponent,
    PagoComponent,
    RegistroFormComponent,
    InicioComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
